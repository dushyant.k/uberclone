import React from 'react';
import {View, Text, Platform} from 'react-native';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
// 1. SetUp Redux
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import Home from './Screens/Home';
import store from './store';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MapScreen from './Screens/MapScreen';
import EatsScreen from './Screens/EatsScreen';
import {KeyboardAvoidingView} from 'react-native';
import tailwind from 'twrnc';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          keyboardVerticalOffset={Platform.OS === 'ios' ? -64 : 0}
          style={tailwind`flex-1`}>
          <Stack.Navigator initialRouteName="Home">
            {/* debug; */}
            <Stack.Screen
              name="Home"
              component={Home}
              options={{headerShown: false}}
            />
            {/* debug; */}
            <Stack.Screen
              name="MapScreen"
              component={MapScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="EatsScreen"
              component={EatsScreen}
              options={{headerShown: false}}
            />
            {/* <Stack.Screen name="Mapscreen" component={Mapscreen} />
          <Stack.Screen name="Eatsscreen" component={EatsScreen} /> */}
          </Stack.Navigator>
        </KeyboardAvoidingView>
      </NavigationContainer>
    </Provider>
  );
}
