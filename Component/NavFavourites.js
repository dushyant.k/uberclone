import React from 'react';
import {FlatList} from 'react-native';
import {StyleSheet, Text, View} from 'react-native';

import {Icon} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import tw from 'twrnc';
Icon;
const NavFavourites = () => {
  const data = [
    {
      id: '123',
      icon: 'home',
      location: 'Home',
      destination:
        'Times Of India Press Road, Shyamal, Ahmedabad, Gujarat, India',
    },
    {
      id: '456',
      icon: 'briefcase',
      location: 'Work',
      destination:
        'UPSQUARE SOLUTIONS PRIVATE LIMITED, opp. Mantavya, Bodakdev, Ahmedabad, Gujarat, India',
    },
  ];
  return (
    <FlatList
      data={data}
      keyExtractor={item => item.id}
      ItemSeparatorComponent={() => (
        <View style={(tw`bg-gray-200`, {height: 0.5})} />
      )}
      renderItem={({item: {location, destination, icon}}) => (
        <TouchableOpacity style={tw`flex-row items-center p-5`}>
          <Icon
            style={tw`mr-4 p-3 bg-gray-300 rounded-full `}
            name={icon}
            type="ionicon"
            color="white"
            size={18}
          />
          <View>
            <Text style={tw`font-semibold text-lg`}>{location}</Text>
            <Text style={tw`text-gray-500 w-64 overflow-hidden`}>
              {destination}
            </Text>
          </View>
        </TouchableOpacity>
      )}
    />
  );
};

export default NavFavourites;

const styles = StyleSheet.create({});
