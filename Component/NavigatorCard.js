import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements/dist/icons/Icon';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useDispatch, useSelector} from 'react-redux';
import tailwind from 'twrnc';
import {selectDestination, setDestination} from '../Slices/navReducer';
import NavFavourites from './NavFavourites';
import tw from 'twrnc';
import encodedData from '../encodedData';
const NavigatorCard = ({navigation}) => {
  const destination = useSelector(selectDestination);

  const dispatch = useDispatch();
  return (
    <SafeAreaView style={tailwind`bg-white flex-1`}>
      <Text style={tailwind`text-center py-5 text-xl`}>
        Good Morning, Dushyant
      </Text>
      <View style={tailwind`border-t border-gray-200 flex-shrink`}>
        <View>
          <GooglePlacesAutocomplete
            placeholder="Where To?"
            styles={{
              container: {
                flex: 0,
                paddingTop: 20,
                backgroundColor: 'white',
              },
              textInput: {
                fontSize: 18,
                backgroundColor: '#DDDDDF',
                borderRadius: 0,
              },
              textInputContainer: {
                paddingHorizontal: 20,
                paddingBottom: 0,
              },
            }}
            enablePoweredByContainer={false}
            minLength={2}
            returnKeyType={'Search'}
            query={{
              key: encodedData,
              language: 'en',
            }}
            onPress={(data, details = null) => {
              console.log(data);
              console.log(details);
              // 'details' is provided when fetchDetails = true
              dispatch(
                setDestination({
                  location: details.geometry.location,
                  description: data.description,
                }),
              );
              navigation.navigate('RideOptionCard');
            }}
            fetchDetails={true}
            nearbyPlacesAPI="GooglePlacesSearch"
            debounce={400}
          />
        </View>
        <NavFavourites />
      </View>

      <View
        style={tw`flex flex-row bg-white justify-evenly py-2 mt-auto border-t border-gray-100`}>
        <TouchableOpacity
          disable={!destination}
          onPress={() => {
            navigation.navigate('RideOptionCard');
          }}
          style={tw`flex flex-row justify-between bg-black w-24 px-4 py-3 rounded-full`}>
          <Icon name="car" type="font-awesome" color="white" size={16} />
          <Text style={tw`text-white text-center`}>Riders</Text>
        </TouchableOpacity>
        <TouchableOpacity
          disable={!destination}
          style={tw`flex flex-row justify-between w-24 px-4 px-4 py-3 rounded-full`}>
          <Icon
            name="fast-food-outline"
            type="ionicon"
            color="black"
            size={16}
          />
          <Text style={tw`text-center`}>Eats</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default NavigatorCard;
