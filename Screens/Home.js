import React from 'react';
import {View, Text, Image, SafeAreaView} from 'react-native';
import tw from 'twrnc';
import NavOptions from '../Component/NavOptions';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useDispatch} from 'react-redux';
import {setDestination, setOrigin} from '../Slices/navReducer';
import NavFavourites from '../Component/NavFavourites';
import encodedData from '../encodedData';
export default function Home() {
  const dispatch = useDispatch();
  const [locData, setLocData] = React.useState(false);
  return (
    <SafeAreaView style={tw`bg-white h-full`}>
      <View style={tw`p-5`}>
        <Image
          style={{height: 100, width: 100, resizeMode: 'contain'}}
          source={{
            uri: 'https://upload.wikimedia.org/wikipedia/commons/c/cc/Uber_logo_2018.png',
          }}
        />
        <GooglePlacesAutocomplete
          placeholder="Where From?"
          styles={{
            container: {
              flex: 0,
            },
            textInput: {
              fontSize: 18,
            },
          }}
          enablePoweredByContainer={false}
          minLength={2}
          returnKeyType={'Search'}
          query={{
            key: encodedData,
            language: 'en',
          }}
          onPress={(data, details = null) => {
            console.log(data);
            console.log(details);
            // 'details' is provided when fetchDetails = true
            dispatch(
              setOrigin({
                location: details.geometry.location,
                description: data.description,
              }),
            );
            dispatch(setDestination(null));
            setLocData(true);
          }}
          fetchDetails={true}
          nearbyPlacesAPI="GooglePlacesSearch"
          debounce={400}
        />
        {/* {locData ? <NavOptions /> : <Text>Choose Location First</Text>} */}
        <NavOptions />
        <NavFavourites />
      </View>
    </SafeAreaView>
  );
}
