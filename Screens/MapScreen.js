import {useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'react-native-elements';
import tw from 'twrnc';
import Map from '../Component/Map';
import NavigatorCard from '../Component/NavigatorCard';
import RideOptionCard from '../Component/RideOptionCard';
export default function MapScreen() {
  const Stack = createNativeStackNavigator();
  const navigation = useNavigation();
  return (
    <View style={tw`h-full`}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Home');
        }}
        style={tw`bg-gray-100 absolute top-16 left-8 z-50 p-3 rounded-full shadow-lg`}>
        <Icon name="menu" />
      </TouchableOpacity>
      <View style={tw`h-1/2 bg-red-100`}>
        <Map />
      </View>
      <View style={tw`h-1/2`}>
        <Stack.Navigator>
          <Stack.Screen
            name="NavigatorCard"
            component={NavigatorCard}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="RideOptionCard"
            component={RideOptionCard}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
