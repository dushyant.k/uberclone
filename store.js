import {configureStore} from '@reduxjs/toolkit';
import navReducer from './Slices/navReducer.js';

export default store = configureStore({
  reducer: {
    nav: navReducer,
  },
});
